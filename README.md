## Hello! ##
Thanks for checking out my project! This is my first attempt at a complete game, inspired by handmade-oriented game devs (including but not exclusive to Handmade Hero), in a specific style that I have in mind. This project is meant partially as a resume, partially as my personal classroom, and partially as a short step towards fulfilling a lifelong dream of mine: to create simulation-like games I wished I could play when I was a kid!

### Author ###
Benjamin Marquardt

www.benjaminmarquardt.com
www.freneticetchings.com

### Info
Languages: C++

Technology: OpenGL, GLFW, GLAD, GLEW

Build tools: g++

### Filelist ###
main: [none, until platform code is finished]

platform: win64-main.cpp, win64-platform.cpp, win64-platform.h
	
other: README.md, Kowloon.jpg, progress.txt

### History ###
I've mentioned Handmade Hero as one inspiration source - and that project is greatly responsible for pushing me to start this endeavor - but it is only an inspiration in a general, work-ethic-plus-raw-code kind of way. The content for this game is inspired by the obvious: Kowloon Walled City, a little section of Hong Kong that existed as a "densely populated, largely ungoverned settlement in Kowloon City," to quote from Wikipedia. Life persisted here apart from any centralized governing body, largely dependent on cooperation and voluntary exchange with those around you, somewhat dependent on exploitation of locally monopolized resources and utilities, and ultimately in a state of maximum freedom. 

In such a mobile web, many would assume that disorder or violence would immediately seize those living in such an environment, but accounts of those that lived and worked and built a community there were overwhelmingly positive about their experiences, and regretted the fact that Kowloon was eventually ransacked and torn down in its entirety by local law enforcement. 

As in any free community, exploitation by the non-free would have made the area look like a pit of iniquity - a source for drugs, prositutes, and other illicit goods and services denied elsewhere. However, through the ingenuity of its inhabitants, Kowloon Walled City maintained at least a modicum of security and escape from poverty for its inhabitants by being a bolthole for those rejected by society and a haven for the rebels. As such, Kowloon Walled City was diverse, dynamic, and bursting with life. In lieu of high culture there was creative industry; in lieu of leisure there was furious activity followed by furious play; in lieu of safety there was a community net; and in lieu of a governing body, there was the Council. 

### Objectives ###
The object of this project is to simulate the complexities of the interactions, livelihoods, and codependencies of similar people living in a similar environment to Kowloon Walled City. My goal is to do this in such a way as to be uncompromising about the good and bad qualities of such a life, but also to provide entertainment in the different ways this environment could be interactive and self-perpetuating. My goal, in fact, is to do the above in a way that makes the result ultimately replayable/perpetual without being so tied to history that I cannot bring balance to the interacting forces within the simulation.

And yes, the ultimate goal is to create a deep and complex simulation, or a system of interconnected simulations that will produce a rich experience for players to participate in. Part of the philosophy behind my idea concerns my frustration with the current market in terms of its obsession with visual and physics simulation, neglecting a wide variety of complex interactive but behavioral simulations that would make the worlds players explore far more real - and far more variant in the axes of possible interactivity and exploration. Simulating electrical systems, computer systems, and systems like plumbing; simulating complex variable behavior based on mood and emotions rather than simple needs (or, worse, binary behaviors - in-danger or out-of-danger dichotomies); simulating organizational hierarchies would be far more complex, but would create the possibility for economic simulation, political simulation, and would create the possibility for emergent gameplay far beyond the scope of static story-writing. But that's highly theoretical - I would want to begin with 'simple' systems first.

To summarize, I'm interested in changing the focus of interactivity from static story-writing with a splash of simulation to highly involved simulation engineered to produce interesting stories as the player winds their way through the interactive environment. Let the player find their story; my job is to give them the perfect playground with maximum freedom, without making the constraints so tight or so loose that they disrupt that goal. 

### Current Progress ###
The 'Progress.txt' file (currently only in the 'dev' branch) is where I'll be keeping a record of my TODO list, future features, and the direction in which I'm currently trying to drag the project. As of this very moment, I'm concentrating on Windows platform code, with the idea that the platform and main code will be separate from the rest of the program so that in future I'll be able to create the platform-specific equivalents engineered to link up with the rest of the main code, which will stand on its own regardless of the platform. 
	
### License ###
None to speak of yet, but I'd like to set it up under a free-as-in-beer license with optional donations as long as the project is running. Once it's finished, I'll probably either set it up in a free-as-in-speech license or sell it to someone.