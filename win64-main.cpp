//entry-point - main.cpp

#include <exception>
#include <stdlib.h>
#include <stdio.h>
//Platform-specific References
using namespace std;
#include "win64-platform.h"

int main(int num_args, char** arguments)
{
	bool running = true; 

	char userInput[64];
	
	printf("Initializing Windows platform code.\n");
	
	WindowData window;
	KeyState keystate;

	if(!winPlatformInit(&running, &window, &keystate))
	{
		printf("Problem initializing Windows platform code! Closing...\n");
		scanf(userInput);
		running = false;
	}
	else
	{
		printf(test());
		printf("Window created successfully.\n");
	}

	for(int x = 1; x < num_args; x++)
	{
		printf("Argument %d: %s\n", x, arguments[x]);
	}

	while(running)
	{
		window.Callback.pollEvents(&window);
		if(window.window_should_close || keystate.ESC)
		{
			window.Callback.window_close(window.Window);
		}
	}

	printf("Completed tasks. Closing down now.\n");
	printf("Press any key to continue...\n");
	scanf(userInput);

	return 0;
}
