#include "win64-platform.h"
#include <exception>
#include <iostream>

bool* internal_running;
KeyState* internal_keystate;

const char * test()
{
	return "'Win64-platform.cpp' is working correctly!\n";
}

void CloseWindow(GLFWwindow* window)
{
	std::cout << "Destroying the window due to close request.\n" << std::endl;
	*internal_running = false;
	glfwDestroyWindow(window);
}

void PollWindowEvents(WindowData* window)
{
	glfwPollEvents();
	if(glfwWindowShouldClose(window->Window))
	{
		std::cout << "Polling suggests the window should close.\n" << std::endl;
		window->window_should_close = true;
	}
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	switch(scancode)
	{
		case GLFW_KEY_ESCAPE:			{ internal_keystate->ESC = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_ENTER: 			{ internal_keystate->ENTER = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_LEFT_SHIFT: 		{ internal_keystate->L_SHIFT = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_RIGHT_SHIFT: 		{ internal_keystate->R_SHIFT = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_LEFT_CONTROL: 	{ internal_keystate->L_CTRL = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_RIGHT_CONTROL: 	{ internal_keystate->R_CTRL = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_LEFT: 			{ internal_keystate->L_ARROW = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_RIGHT: 			{ internal_keystate->R_ARROW = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_DOWN: 			{ internal_keystate->D_ARROW = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_UP: 				{ internal_keystate->U_ARROW = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_TAB: 				{ internal_keystate->TAB = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_BACKSPACE:		{ internal_keystate->BACKSPACE = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_DELETE:			{ internal_keystate->DEL = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_END: 				{ internal_keystate->END = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_PAGE_UP:			{ internal_keystate->PAGE_UP = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_PAGE_DOWN:		{ internal_keystate->PAGE_DOWN = (action == GLFW_PRESS); break;		} 
		case GLFW_KEY_PAUSE:			{ internal_keystate->PAUSE = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_HOME:				{ internal_keystate->HOME = (action == GLFW_PRESS); break;			} 
		case GLFW_KEY_A: 				{ internal_keystate->A = (action == GLFW_PRESS); break;		 		} 
		case GLFW_KEY_B: 				{ internal_keystate->B = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_C: 				{ internal_keystate->C = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_D: 				{ internal_keystate->D = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_E: 				{ internal_keystate->E = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_F: 				{ internal_keystate->F = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_G: 				{ internal_keystate->G = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_H: 				{ internal_keystate->H = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_I: 				{ internal_keystate->I = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_J: 				{ internal_keystate->J = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_K: 				{ internal_keystate->K = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_L: 				{ internal_keystate->L = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_M: 				{ internal_keystate->M = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_N: 				{ internal_keystate->N = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_O: 				{ internal_keystate->O = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_P: 				{ internal_keystate->P = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_Q: 				{ internal_keystate->Q = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_R: 				{ internal_keystate->R = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_S: 				{ internal_keystate->S = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_T: 				{ internal_keystate->T = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_U: 				{ internal_keystate->U = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_V: 				{ internal_keystate->V = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_W: 				{ internal_keystate->W = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_X: 				{ internal_keystate->X = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_Y: 				{ internal_keystate->Y = (action == GLFW_PRESS); break;				} 
		case GLFW_KEY_Z: 				{ internal_keystate->Z = (action == GLFW_PRESS); break;				} 
		default:{ /*do nothing*/}
	}
}

int winPlatformInit(bool *main_running, WindowData* windowData, KeyState* keystate)
{
	internal_running = main_running;
	internal_keystate = keystate;
	int success = 1;
	//initialize the window and grab handle

	windowData->Height = 800;
	windowData->Width = 600;
	windowData->Callback.window_close = CloseWindow;
	windowData->Callback.pollEvents = PollWindowEvents;
	windowData->window_should_close = false;

	try
	{
		if(!glfwInit())
		{
			std::cout << "GLFW initialization failed with error %d!\n" <<  glfwGetError(NULL) << std::endl;
		}

		windowData->Window = glfwCreateWindow(
			windowData->Height, 
			windowData->Width, 
			"Kowloon", 			//Window title
			NULL, NULL);
		glfwMakeContextCurrent(windowData->Window);
	}
	catch(const std::exception e)
	{
		printf("INIT ERROR: %s\n", e.what());
		success = 0;
	}

	//set key callback

	glfwSetKeyCallback(windowData->Window, key_callback);

	std::cout << "Window initialized without complications.\n" << std::endl;
	//initialize buffers
	//hook up event handlers
	//initialize and populate gamestate object
	return success;
}