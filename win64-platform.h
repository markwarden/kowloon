#ifndef WIN64_PLATFORM_HEADER
#define WIN64_PLATFORM_HEADER
	#include "glad\gl.h"
	#include "GLFW\glfw3.h"
	#include <functional>
	
	/*
		DEBUG
	*/

	const char* test();



	/*							/
	/		  =GRAPHICS=		/
	/		 					/
	/							/
	/    		   	   	   	   */

	/*
		WINDOW
	*/

	struct WindowData;

	typedef struct WindowCallbacks {
		void (*pollEvents)(WindowData *);
		GLFWwindowclosefun window_close;
		GLFWwindowsizefun window_size;
		GLFWwindowposfun window_position;
		GLFWwindowrefreshfun window_refresh;
		GLFWwindowfocusfun window_focus;
		GLFWwindowiconifyfun window_iconify;
		GLFWwindowmaximizefun window_maximize;
		GLFWframebuffersizefun framebuffer_resize;
		GLFWwindowcontentscalefun window_contentscale;
	} WindowCallbacks;

	typedef struct WindowData {
		GLFWwindow* Window;
		int Width;
		int Height;
		WindowCallbacks Callback;
		bool window_should_close;
		bool running;
	} WindowData;

	typedef struct WindowMessageQueue {

	} WinMessageQueue;

	/*
		BUFFER
	*/

	/*
		ASSETS	
	*/

	/*							/
	/		 =GAME STATE=		/
	/		 					/
	/							/
	/    		   	   	   	   */

	/*
		KEYSTATE
	*/
	typedef struct KeyState {
		bool L_SHIFT = false;
		bool R_SHIFT = false;
		bool L_CTRL = false;
		bool R_CTRL = false;
		bool L_ARROW = false;
		bool R_ARROW = false;
		bool U_ARROW = false;
		bool D_ARROW = false;
		bool TAB = false;
		bool ESC = false;
		bool BACKSPACE = false;
		bool DEL = false;
		bool END = false;
		bool PAGE_UP = false;
		bool PAGE_DOWN = false;
		bool HOME = false;
		bool ENTER = false;
		bool PAUSE = false;

		bool A = false;
		bool B = false;
		bool C = false;
		bool D = false;
		bool E = false;
		bool F = false;
		bool G = false;
		bool H = false;
		bool I = false;
		bool J = false;
		bool K = false;
		bool L = false;
		bool M = false;
		bool N = false;
		bool O = false;
		bool P = false;
		bool Q = false;
		bool R = false;
		bool S = false;
		bool T = false;
		bool U = false;
		bool V = false;
		bool W = false;
		bool X = false;
		bool Y = false;
		bool Z = false;
	} KeyState;
	/*
		MOUSESTATE
	*/

	/*
		VIEW
	*/

	/*
		INIT
	*/

	int winPlatformInit(bool *, WindowData *, KeyState *);
	void key_callback(GLFWwindow*, int, int, int, int);
#endif